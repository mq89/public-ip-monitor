# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Print current database with `-p`.

### Changed

- Write IP to file only if it has changed.

### Fixed

- Fix a case where IP was not assigned.

## [1.1.0] - 2020-03-25

### Changed

- Change working directory to location of script.

## [1.0.0] - 2020-03-25

### Added

- Resolve IP of supervised domain.
- Save IP to database.
- Log via syslog.
- Save time of last run.
- Save last resolved IP.
- Configure via default.yml and local.yml

[Unreleased]: https://gitlab.com/Mq_/public-ip-monitor/compare/v1.0.0...master
[1.1.0]: https://gitlab.com/Mq_/public-ip-monitor/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/Mq_/public-ip-monitor/tags/v1.0.0