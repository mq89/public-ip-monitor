# Public IP Monitor Script

Resolves the IP of a hostname and saves it to a database.

## Installation

Only uses common python packages, so it should work out of the box.
Otherwise `pip install -r requirements.txt` installs all dependencies.

## Running

Execute `public-ip-monitor.py` at the intervall of your choice.
E.g., via crontab.

Execute `public-ip-monity.py -p` to print the current database instead.

## Configuration

A default configuration can be found in `config/default.yml`.
If you wish to change something, create `config/local.yml` and define the config you want to change there.
Everything in `local.yml` overwrites the configuration of `default.yml`.

